﻿#pragma strict

var snd : AudioClip;	
var explosion : Transform;
function Start () {

}

function Update () {

}

function OnTriggerEnter(coll : Collider) {

	Instantiate(explosion, transform.position, Quaternion.identity);
	AudioSource.PlayClipAtPoint(snd, transform.position);
	Destroy(gameObject);
	
	if(coll.gameObject.tag == "WALL") {
		Instantiate(explosion, coll.transform.position, Quaternion.identity);
		AudioSource.PlayClipAtPoint(snd, transform.position);	
		Destroy(coll.gameObject);
		Destroy(gameObject);
		
		}else if(coll.gameObject.tag == "ENEMY") {
		jsScore.hit++;
		if(jsScore.hit > jsScore.MAXSCORE){
		Destroy(coll.transform.root.gameObject);
		//승리화면으로 분기
		Application.LoadLevel(2);
		}
		
		}else if(coll.gameObject.tag == "TANK") {
		jsScore.lost++;
		if(jsScore.lost > jsScore.MAXSCORE){
		//패배화면으로 분기
		Application.LoadLevel(3);
		}
	}
}